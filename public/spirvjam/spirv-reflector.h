
#ifndef SPIRVJAM_SPIRV_REFLECTOR_H_
#define SPIRVJAM_SPIRV_REFLECTOR_H_

#include "types.h"
#include "common.h"
#include <functional>

namespace SpirvJam
{
	bool load(const std::string& path);
} // ns SpirvJam

#endif // SPIRVJAM_SPIRV_REFLECTOR_H_
