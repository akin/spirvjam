
#ifndef SPIRVJAM_COMMON_H_
#define SPIRVJAM_COMMON_H_

// enable SSE2
#define GLM_FORCE_SSE2 1
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES 1

#define GLM_ENABLE_EXPERIMENTAL 1
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <vector>
#include <memory>
#include <chrono>
#include <cstdint>

using Matrix = glm::mat4;
using Float = float;
using Time = float;

using Timings = std::vector<size_t>;
using Clock = std::chrono::high_resolution_clock;

#endif // SPIRVJAM_COMMON_H_
