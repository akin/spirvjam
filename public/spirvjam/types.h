
#ifndef SPIRVJAM_TYPES_H_
#define SPIRVJAM_TYPES_H_

#include "common.h"
#include <vector>
#include <unordered_map>
#include <functional>

namespace SpirvJam 
{
	enum class Ops {
		Empty
	};

	using CodeCallback = std::function<void(uint16_t, uint32_t*, size_t)>;
	using Parsers = std::unordered_map<uint16_t, CodeCallback>;

} // ns SpirvJam

#endif // SPIRVJAM_TYPES_H_
