
#include "inst.h"
#include <iostream>
#include <string>
#include <vulkan/spirv.hpp>

namespace SpirvJam
{
void loadCommon(Parsers& parsers)
{

	parsers[spv::Op::OpCapability] = [](uint16_t size, uint32_t* data, size_t max)
	{
		std::cout << "OpCapability ";
		switch (data[0])
		{
		case spv::Capability::CapabilityMatrix: std::cout << "Matrix" << std::endl; break;
		case spv::Capability::CapabilityShader: std::cout << "Shader" << std::endl; break;
		default:
			std::cout << "? " << data[0] << std::endl;
		}
	};

	parsers[spv::Op::OpExtInstImport] = [](uint16_t size, uint32_t* data, size_t max)
	{
		std::cout << "OpExtInstImport ";
		std::cout << "ID [" << data[0] << "] ";

		// Strings are null terminated!
		// Why -2?? TODO!
		size_t len = (size - 2) * sizeof(uint32_t);

		char *cstr = (char*)(data + 1);
		std::string name((char*)(data + 1), len);
		std::cout << "\"" << name << "\"" << std::endl;
	};

	parsers[spv::Op::OpMemoryModel] = [](uint16_t size, uint32_t* data, size_t max)
	{
		std::cout << "OpMemoryModel ";
		std::cout << "Addressing model: ";
		switch (data[0])
		{
		case spv::AddressingModel::AddressingModelLogical: std::cout << "Logical "; break;
		case spv::AddressingModel::AddressingModelPhysical32: std::cout << "Physical_32 "; break;
		case spv::AddressingModel::AddressingModelPhysical64: std::cout << "Physical_64 "; break;
		default:
			std::cout << "? " << data[0] << " ";
		}

		std::cout << "Memory model: ";
		switch (data[1])
		{
		case spv::MemoryModel::MemoryModelSimple: std::cout << "Simple" << std::endl; break;
		case spv::MemoryModel::MemoryModelGLSL450: std::cout << "GLSL450" << std::endl; break;
		case spv::MemoryModel::MemoryModelOpenCL: std::cout << "OpenCL" << std::endl; break;
		case spv::MemoryModel::MemoryModelVulkanKHR: std::cout << "VulkanKHR" << std::endl; break;
		default:
			std::cout << "? " << data[0] << std::endl;
		}
	};

	parsers[spv::Op::OpEntryPoint] = [](uint16_t size, uint32_t* data, size_t max)
	{
		std::cout << "OpEntryPoint ";
		std::cout << "Execution model: ";
		switch (data[0])
		{
		case spv::ExecutionModel::ExecutionModelVertex: std::cout << "Vertex "; break;
		case spv::ExecutionModel::ExecutionModelTessellationControl: std::cout << "TessellationControl "; break;
		case spv::ExecutionModel::ExecutionModelTessellationEvaluation: std::cout << "TessellationEvaluation "; break;
		case spv::ExecutionModel::ExecutionModelGeometry: std::cout << "Geometry "; break;
		case spv::ExecutionModel::ExecutionModelFragment: std::cout << "Fragment "; break;
		case spv::ExecutionModel::ExecutionModelGLCompute: std::cout << "GLCompute "; break;
		case spv::ExecutionModel::ExecutionModelKernel: std::cout << "Kernel "; break;
		case spv::ExecutionModel::ExecutionModelTaskNV: std::cout << "TaskNV "; break;
		case spv::ExecutionModel::ExecutionModelMeshNV: std::cout << "MeshNV "; break;
		case spv::ExecutionModel::ExecutionModelRayGenerationNVX: std::cout << "RayGenerationNVX "; break;
		case spv::ExecutionModel::ExecutionModelIntersectionNVX: std::cout << "IntersectionNVX "; break;
		case spv::ExecutionModel::ExecutionModelAnyHitNVX: std::cout << "AnyHitNVX "; break;
		case spv::ExecutionModel::ExecutionModelClosestHitNVX: std::cout << "ClosestHitNVX "; break;
		case spv::ExecutionModel::ExecutionModelMissNVX: std::cout << "MissNVX: "; break;
		case spv::ExecutionModel::ExecutionModelCallableNVX: std::cout << "CallableNVX: "; break;
		default:
			std::cout << "? " << data[0] << " ";
		}

		std::cout << "ID [" << data[1] << "] ";


		size_t len = (size - 5) * sizeof(uint32_t);

		std::string name((char*)(data + 2), len);
		std::cout << "\"" << name << "\"" << std::endl;
	};
}
} // ns SpirvJam
