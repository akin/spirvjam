
#ifndef SPIRVJAM_INST_H_
#define SPIRVJAM_INST_H_

#include <spirvjam/types.h>

namespace SpirvJam
{
	void loadCommon(Parsers& parsers);
} // ns SpirvJam

#endif // SPIRVJAM_INST_H_